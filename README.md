# TTGO LoRa32 V2.1 1.6

Housing and micropython library for TTGO LoRa32 V2.1 1.6

## Micropython lib
#### buildin_led  
Returns the pin of the built-in led
#### init_oled()
Creates objects to draw on the oled.  
Lazy loading is used for this.
#### init_sd_card()
Creates an object to access the sd carte.  
The sd card can be [mounted](https://docs.micropython.org/en/latest/library/machine.SDCard.html).  
Should a problem arise such as there is no sd card, an exception will be thrown (OSError).  
#### read_battery_volt()  
Reads out the battery voltage.  
#### read_battery_level()
Calculates the percentage with battery_min_volt and battery_max_volt.  
It's not accurate because it uses a linear slope.  
#### init_lora()
Comming soon  

### Settings
#### battery_min_volt (float)
To set the min voltage.  
Should be done immediately after creation if the value needs to be changed.  
default: `3.4V`

#### battery_max_volt (float)
To set the max voltage.  
Should be done immediately after creation if the value needs to be changed.  
default: `4.2V`

---

### Symbols
`oled` must be an object that has a `pixel(x,y,col)` function.  
`xs`, `ys` are the starting points.  
Most of the symbols are `8*8`pixels, only the battery needs `8*14`.  
`col` which color (1 or 0).    

#### area(oled, xs, ys, w, h, col)
Draws a filled rectangle, can also be used to draw lines.  
`w` and `h` indicate how wide and high the rectangle should be.  

#### draw_battery(oled,xs,ys,p,col)
Draws a battery. `p` is the percentage(0-100).

#### draw_bluetooth(oled,xs,ys,col)
Draws a bluetooth symbol.

#### draw_lora(oled,xs,ys,p,col)
Draws an antenna symbol. `p` is the percentage(0-100).

#### draw_sd_card(oled,xs,ys,col)
Draws a sd card symbol.

---

### Required libraries  

**Micropython:** https://micropython.org  
**SD Card:** https://github.com/micropython/micropython/blob/master/drivers/sdcard/sdcard.py  
**OLED:** https://github.com/adafruit/micropython-adafruit-ssd1306/blob/master/ssd1306.py  
**LORA:** Comming soon

---

## Housing
![1](img/1.png)
![2](img/2.png)
![3](img/3.png)
![4](img/4.png)
