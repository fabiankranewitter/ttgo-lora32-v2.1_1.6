class Symbols:
    
    def area(oled, xs, ys, w, h, col):
        for x in range(xs,xs+w):
            for y in range(ys,ys+h):
                oled.pixel(x,y,col)

    
    def draw_battery(oled,xs,ys,p,col):
        if(p<0):
            p=0
        if(p>100):
            p=100
        h = 8
        w = 14
        pw = w-4
        ph = h-4
        xp = p/10
        for x in range(xs+w,xs+w+2):
            for y in range(ys+2,ys+h-2):
                oled.pixel(x,y,col)
                
        for x in range(xs,xs+w):
            for y in range(ys,ys+h):
                if(x==xs or x==xs+w-1 or y==ys or y==ys+h-1 or x >= xs+2 and y >= ys+2 and x <= xs+xp+1 and y <= ys+h-3):
                    oled.pixel(x,y,col)

    def draw_bluetooth(oled,xs,ys,col):
        h = 8
        w = 8
        for x in range(xs,xs+w):
            for y in range(ys,ys+h):
                if(x==xs+3 or x==xs+2 and (y==ys+3 or y==ys+4) or x==xs+4 and (y==ys+1 or y==ys+h-2 or y==ys+3 or y==ys+4) or x==xs+5 and (y==ys+2 or y==ys+h-3) or x==xs+1 and (y==ys+2 or y==ys+5)):
                    oled.pixel(x,y,col)
    
    def draw_lora(oled,xs,ys,p,col):
        h = 8
        w = 8
        for x in range(xs,xs+w):
            for y in range(ys,ys+h):
                if(y==ys+h-1 or ((x==xs or x==xs+1) and y>=ys+h-3 and p>25) or ((x==xs+3 or x==xs+4) and y>=ys+h-5 and p>50) or ((x==xs+6 or x==xs+7) and y>=ys-h-7 and p>75)):
                    oled.pixel(x,y,col)
    
    def draw_sd_card(oled,xs,ys,col):
        h = 8
        w = 8
        for x in range(xs,xs+w):
            for y in range(ys,ys+h):
                if(x==xs+1 and y>ys+1 or x==xs+2 and (y==ys+h-1 or y==ys+1) or x==xs+3 and (y==ys+h-1 or y==ys or y==ys+1 or y==ys+2) or x==xs+4 and (y==ys+h-1 or y==ys) or x==xs+5 and (y==ys+h-1 or y==ys or y==ys+1 or y==ys+2) or x==xs+w-2):
                    oled.pixel(x,y,col)
