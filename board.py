from machine import Pin, ADC

class Board:
    
    def __init__(self):
        # led
        self._buildin_led = Pin(25,Pin.OUT)
        # battery
        self.battery_min_volt = 3.4 #LM6206-3.3 dropout volt (100mA) max 680mV, 680mV+3.3V =~3.4V
        self.battery_max_volt = 4.2 #TP4054 max charage volt
        self._battery_adc = ADC(Pin(35))
        self._battery_adc.atten(ADC.ATTN_11DB)
        self._battery_adc.width(ADC.WIDTH_12BIT)
        # oled
        self._i2c_oled = None
        self._oled = None
        # sd card
        self._spi_sd = None
        self._sd = None
        #lora
        self._spi_lora = None
        self._lora = None

    @property
    def buildin_led(self):
        return self._buildin_led
    
    def init_oled(self):
        if(self._i2c_oled == None):
            from machine import SoftI2C
            self._i2c_oled = SoftI2C(scl=Pin(22), sda=Pin(21))
        if(self._oled == None):
            import ssd1306
            self._oled = ssd1306.SSD1306_I2C(128, 64, self._i2c_oled)
        return self._oled
    
    def init_sd_card(self):
        if(self._spi_sd == None):
            from machine import SoftSPI
            self._spi_sd = SoftSPI(sck=Pin(14), mosi=Pin(15), miso=Pin(2))
        if(self._sd == None):
            import sdcard
            self._sd = sdcard.SDCard(self._spi_sd, Pin(13))
        return self._sd

    def read_battery_volt(self):
        # 12 bit -> 4096, 2 -> voltage divider(100k/100k), 3.3 -> 3.3Volt, 1.1 -> ADC VREF
        return self._battery_adc.read() / 4096 * 2 * 3.3 * 1.1
    
    def read_battery_level(self):
        volt = self.read_battery_volt()
        if (volt >= self.battery_max_volt):
            return 100
        if (volt <= self.battery_min_volt):
            return 0
        return (volt - self.battery_min_volt) * 100 / (self.battery_max_volt - self.battery_min_volt);
    
    def init_lora(self):
        if(self._spi_lora == None):
            self._spi_lora = SoftSPI(sck=Pin(5), mosi=Pin(27), miso=Pin(19))
        return self._lora
    
